<!Doctype html>
<html>
<head>
	<title>Ejercicios PHP</title>

</head>

<body>
	<h1>Ejercicios de Comparacion, logicos y Operadores Aritmeticos en PHP</h1>

	<?php

		$i=9;
		$f=33.5;
		$c='X';

		echo ($i >= 6) && ($c == 'X'); // false
		echo ($i >= 6) || ($c == 12), "<br>";//resultado true
		echo ($f < 11) && ($i > 100), "<br>";//resultado false
		echo ($c != 'p') || (($i + $f) <= 10), "<br>"; //resultado true
		echo $i + $f <= 10, "<br>";	//resultado false
		echo $i >= 6 && $c == 'X', "<br>";//resultado false
		echo $c != 'p' || $i + $f <= 10, "<br>"; //resultado true

	?>


</body>

</html>